import 'package:flutter/material.dart';

class ShowLetterScreen extends StatefulWidget {
  const ShowLetterScreen({super.key});

  @override
  State<ShowLetterScreen> createState() => _ShowLetterScreenState();
}

class _ShowLetterScreenState extends State<ShowLetterScreen> {
  int codeCharInicial = 65;
  int codeCharFinal = 90;
  int codeChar = 65;
  String letterShow = "A";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Challenge #1 - Show Letter'),
        ),
        body: crearBody(),
        floatingActionButton: crearBotones(),
      ),
    );
  }

  Widget crearBody() {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          letterShow,
          style: const TextStyle(fontSize: 170, fontWeight: FontWeight.bold),
        ),
      ],
    ));
  }

  Widget crearBotones() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        FloatingActionButton(
          onPressed: previousLetter,
          child: const Icon(Icons.arrow_back_ios_new_outlined),
        ),
        const SizedBox(
          width: 10,
        ),
        FloatingActionButton(
          onPressed: nextLetter,
          child: const Icon(Icons.arrow_forward_ios_outlined),
        ),
      ],
    );
  }

  void nextLetter() => setState(() {
        if (codeChar != codeCharFinal + 1) {
          letterShow = String.fromCharCode(codeChar);
          codeChar++;
        } else {
          codeChar = codeCharInicial;
          letterShow = String.fromCharCode(codeChar);
        }
      });

  void previousLetter() => setState(() {
        if (codeChar != codeCharInicial - 1) {
          letterShow = String.fromCharCode(codeChar);
          codeChar--;
        } else {
          codeChar = codeCharFinal;
          letterShow = String.fromCharCode(codeChar);
        }
      });
}
